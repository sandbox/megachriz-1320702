Ubercart Discount Coupons Auto Apply
---------------------------
With this module you have the ability to automatically apply discount coupons to orders
at checkout. Because Ubercart Discount Coupons is designed so that you can only apply
*one* discount at a time, this module also applies max. *one* coupon automatically.

When this module is asked to apply coupons it checks which coupons may be applied
automatically, and the first coupon that meets the conditions will be applied.

How it works
---------------------------
The module adds two properties to a coupon:
  * Auto Apply
    A setting whether or not to apply this coupon automatically.
  * Weight
    Used to set the order of auto-apply coupons. The lightest coupon that meets the
    conditions will be applied.

In order to save the data for the new properties, the database schema of uc_coupons is
altered.

On each page load, the module checks if it should automatically apply coupons. If there
is already a coupon applied, it will stop looking. If there is no coupon applied it
will apply the lightest coupon that may be applied automatically that meets the
conditions.

Auto Apply coupons overview page
---------------------------
On the Auto Apply coupons page you have an overview of all coupons that can be applied
automatically. You can also change the order of the coupons on this page.
admin/store/coupons/auto_apply