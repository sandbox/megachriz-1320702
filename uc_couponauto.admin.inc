<?php
/**
 * @file
 * Admin function for Discount Coupons Auto Apply
 */

/**
 * Display a brief overview of coupons that automatically apply.
 *
 * @param array $form_state
 * @param $view_type
 *   'active' or 'inactive'
 *
 * @return array $form
 *   The form array
 */
function uc_couponauto_display($form_state, $view_type = 'active') {
  _uc_coupon_paypal_check();

  $form['#table_header'] = array(
    array('data' => t('Actions'), 'fieldname' => 'actions'),
    array('data' => t('Name'), 'fieldname' => 'name'),
    array('data' => t('Code'), 'fieldname' => 'code'),
    array('data' => t('Value'), 'fieldname' => 'value'),
    array('data' => t('Weight'), 'fieldname' => 'weight'),
    array('data' => t('Created'), 'fieldname' => 'created'),
    array('data' => t('Valid from'), 'fieldname' => 'valid_from'),
    array('data' => t('Valid until'), 'fieldname' => 'valid_until'),
  );
  $form['#view_type'] = $view_type;

  $result = pager_query('SELECT * FROM {uc_coupons} WHERE status = %d AND auto_apply = %d ORDER BY weight', 20, 0, NULL, $view_type == 'inactive' ? 0 : 1, 1);
  $rows = array();
  while ($coupon = db_fetch_object($result)) {
    $coupon->data = $coupon->data ? unserialize($coupon->data) : array();
    $rows[] = array(
      'actions' => theme('uc_coupon_actions', $coupon),
      'name' => check_plain($coupon->name),
      'code' => check_plain($coupon->code) . ($coupon->bulk ? '* '. t('(bulk)') : ''),
      'value' => uc_coupon_format_discount($coupon),
      'weight' => $coupon->weight,
      'created' => _uc_coupon_format_date($coupon->created, variable_get('uc_date_format_default', 'm/d/Y')),
      'valid_from' => $coupon->valid_from ? _uc_coupon_format_date($coupon->valid_from, variable_get('uc_date_format_default', 'm/d/Y H:iT')) : '-',
      'valid_until' => $coupon->valid_until ? _uc_coupon_format_date($coupon->valid_until, variable_get('uc_date_format_default', 'm/d/Y H:iT')) : '-',
      'coupon' => $coupon,
    );
  }

  $form['coupons'] = array(
    '#tree' => TRUE,
  );
  if (count($rows)) {
    foreach ($rows as $row) {
      $cid = $row['coupon']->cid;
      $form['coupons'][$cid] = array();
      $form['coupons'][$cid]['cid'] = array(
        '#type' => 'value',
        '#value' => $cid,
      );
      foreach ($form['#table_header'] as $column) {
        $fieldname = $column['fieldname'];

        switch ($fieldname) {
          case 'weight':
            $form['coupons'][$cid][$fieldname] = array(
              '#type' => 'weight',
              '#delta' => 50,
              '#default_value' => $row[$fieldname],
              '#attributes' => array('class' => 'uc-couponauto-display-table-ordering'),
            );
            break;
          default:
            $form['coupons'][$cid][$fieldname] = array(
              '#value' => $row[$fieldname],
            );
            break;
        }
      }
    }
  }

  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save'),
  );

  return $form;
}

/**
 * Saves weights of coupons
 *
 * @param array $form
 * @param array $form_state
 *
 * @return void
 */
function uc_couponauto_display_submit($form, &$form_state) {
  foreach ($form_state['values']['coupons'] as $coupon) {
    drupal_write_record('uc_coupons', $coupon, array('cid'));
  }
}

/**
 * Theme the list of the auto-apply coupons
 *
 * @param array $form
 *
 * @return string
 */
function theme_uc_couponauto_display($form) {
  $output = "";

  $header = $form['#table_header'];
  $rows = array();
  foreach (element_children($form['coupons']) as $cid) {
    $row = array();
    foreach ($header as $column) {
      $fieldname = $column['fieldname'];
      $row[] = drupal_render($form['coupons'][$cid][$fieldname]);
    }
    $rows[] = array(
      'data' => $row,
      'class' => 'draggable',
    );
  }

  if (count($rows)) {
    drupal_add_tabledrag('uc-couponauto-display-table', 'order', 'sibling', 'uc-couponauto-display-table-ordering');
    $output = theme('table', $header, $rows, array('width' => '100%', 'id' => 'uc-couponauto-display-table'));
    $output .= theme('pager', NULL, 20);
  }
  else {
    switch ($form['#view_type']) {
      case 'active':
        $output = '<p>'. t('There are currently no active coupons in the system that automatically apply.') .'</p>';
        break;
      case 'inactive':
        $output = '<p>'. t('There are currently no inactive coupons in the system that automatically apply.') .'</p>';
        break;
    }
  }

  $output .= '<p>'. l(t('Add a new coupon.'), 'admin/store/coupons/add') .'</p>';

  $output .= '<br />' . drupal_render($form);
  return $output;
}
